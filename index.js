const DuktapeVM = require("./node_modules/duktape-vm/build/duktape-vm")
  .DuktapeVM;

const express = require("express");
const app = express();
const port = 3000;
const USER_DEFINED_FUNCTION = `Object.assign(body, params)`;
app.use(express.json())

app.post("/payload", (req, res) => {
  DuktapeVM().then(vm => {
    let user_defined_function = req.body.user_function
    let fun_body = `
        Object.defineProperty(Object, "assign", {
          enumerable: false,
          configurable: true,
          writable: true,
          value: function(target) {
            "use strict";
            if (target === undefined || target === null) {
              throw new TypeError("Cannot convert first argument to object");
            }

            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
              var nextSource = arguments[i];
              if (nextSource === undefined || nextSource === null) {
                continue;
              }
              nextSource = Object(nextSource);

              var keysArray = Object.keys(Object(nextSource));
              for (
                var nextIndex = 0, len = keysArray.length;
                nextIndex < len;
                nextIndex++
              ) {
                var nextKey = keysArray[nextIndex];
                var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                if (desc !== undefined && desc.enumerable) {
                  to[nextKey] = nextSource[nextKey];
                }
              }
            }
            return to;
          }
        });
        var body = args[0]
        var params = args[1]
        return JSON.stringify(${user_defined_function})
      `;
    console.log(typeof(req.body.user_data))
    
    let response_body = {service_response: "some data"}
    let value = vm.eval(fun_body, [response_body, req.body.user_data]);
    res.json(JSON.parse(value))
  });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

