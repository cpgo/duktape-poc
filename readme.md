Exemplo usando duktape e duktape-vm para executar javascrip em sandbox

A api exposta pelo duktape é bem limitada, por isso foi necessario fazer um polifill para o Object.assign


```
npm install
node index.js

POST localhost:3000/payload
{
	"user_data": {"gdfgdfg": "asdasdasd"},
	"user_function": "Object.assign(body, params)"
}
```

A resposta do serviço é o objeto `{service_response: "some data"}`
A função no parametro `user_function` será executada onde o parametro `body` é a resposta do serviço e `params` é o payload definido em `user_data`.
